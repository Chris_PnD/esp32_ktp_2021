/*
 * file_system.h
 *
 *  Created on: 30 lis 2021
 *      Author: krzys
 */

#ifndef MAIN_FILE_SYSTEM_H_
#define MAIN_FILE_SYSTEM_H_

#include "esp_vfs.h"
#include "freertos/timers.h"

/* Scratch buffer size */
#define SCRATCH_BUFSIZE  8192

// ADC data buffer size configuration
#define MAX_SIZE_OF_ADC_DATA_BUFFER			50

#define CHANNEL_NAME_STRING_SIZE 			30
#define IP_ADDRESS_STRING_SIZE				16
#define IP_ADDRESS_HEADER_STRING_SIZE		7
#define NODE_NAME_STRING_SIZE				16

// max number of supported channel and timer
#define MAX_SUPPORT_ADC_CHANNEL 10

#define MIN(a, b) ((a) < (b) ? (a) : (b))

//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// TYPE DEFINITION ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

struct file_server_data {
    /* Base path of file storage */
    char base_path[ESP_VFS_PATH_MAX + 1];

    /* Scratch buffer for temporary storage during file transfer */
    char scratch[SCRATCH_BUFSIZE];
};

typedef struct {
	TimerHandle_t 	adc_read_timer_h;
	uint32_t		period;
	char			name[CHANNEL_NAME_STRING_SIZE];
} channel_info_t;

typedef struct {
	channel_info_t channel_info[MAX_SUPPORT_ADC_CHANNEL];
	char server_Addr[IP_ADDRESS_HEADER_STRING_SIZE + IP_ADDRESS_STRING_SIZE];
	char node_name[NODE_NAME_STRING_SIZE];
} node_info_t;

/* Function to start the file server */
esp_err_t init_spiffs(void);

/* Handler to download a file kept on the server */
esp_err_t download_get_handler(httpd_req_t *req);

/* Handler to upload a file onto the server */
esp_err_t upload_post_handler(httpd_req_t *req);

/* Handler to delete a file from the server */
esp_err_t delete_post_handler(httpd_req_t *req);

void web_data_formater(char *buff);

// save configuration
void save_configuration(node_info_t *node_info, uint16_t size);
// read configuration
void read_configuration(node_info_t *node_info, uint16_t size);

#endif /* MAIN_FILE_SYSTEM_H_ */
