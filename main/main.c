/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/timers.h"

#include "driver/gpio.h"
//#include "driver/adc.h"  <- deprecated
#include "esp_adc/adc_oneshot.h"
//#include "esp_adc_cal.h" <- deprecated
#include "esp_adc/adc_cali.h"
#include "esp_adc/adc_cali_scheme.h"

#include "sdkconfig.h"
#include "assert.h"

#include "string.h"
#include "esp_intr_alloc.h"

#include "esp_wifi.h"
#include "nvs_flash.h"
#include "security.h"
#include "esp_http_server.h"
#include "esp_http_client.h"
#include "esp_sntp.h"

#include "file_system.h"

#include "mdns.h" // run -> idf.py add-dependency "espressif/mdns^1.2.1"

#include "esp_netif.h"
//TODO: Potential problem: "Header fields are too long for the server to interpret":
// CONFIG_HTTPD_MAX_REQ_HDR_LEN=1024
////////////////////////////////// macros definition ///////////////////////////////
// pin configuration
#define  LED1_GPIO 							22
#define  LED2_GPIO 							12

#define  BUTTON_PIN 						15
//#define  ADC_INPUT_PIN						ADC_CHANNEL_4 /*pin 32 */
#define  ADC_ATTEN			           		ADC_ATTEN_DB_11


// ADC reference voltage configuration
#define DEFAULT_VREF 						1100

// freertos queue configuration
#define  TEXT_QUEUE_SIZE 					10
#define  TEXT_QUEUE_ELEMENT_SIZE 			50
#define  ADC_DATA_QUEUE_SIZE 				10
#define  ADC_DATA_QUEUE_ELEMENT_SIZE  		sizeof(adc_data_t)
// freertos notification event configuration
#define TEXT_MESSAGE_EVENT 					(1 << 0)
#define ADC_DATA_EVENT						(1 << 1)

// max number of parallel connections
#define MAX_EVENT_SOCKET 5

// header of query for data server
#define JSON_DATA_SERVER_HEADER				"json"
/////////////////////////////////// type definition //////////////////////////////
// type used to store measure variable with time descriptor
typedef struct {
	uint64_t	time_ms;
	int 		raw_data;
	uint8_t     descriptor;
} adc_data_t;

// type used to store connections data
typedef struct {
    httpd_handle_t hd;
    int fd;
} async_resp_arg_t;

/////////////////////////////////// function prototypes ////////////////////////////
static void sender_task( void * pvParameters );
static void data_receiver_task( void * pvParameters );
static void wifi_start(void);
static esp_err_t cmd_get_handler(httpd_req_t *req);
static void set_mdns(void);
static void read_adc_data_buffer(adc_data_t *buffer);
static void send_data_to_data_server(char * buff_out);
////////////////////////// external function prototypes ////////////////////////////
int httpd_default_send(httpd_handle_t hd, int sockfd, const char *buf, size_t buf_len, int flags);

//////////////////////////////////// constant variables ////////////////////////////
const static char hdr[] = "HTTP/1.1 200 OK\nContent-Type: text/event-stream\nCache-Control: no-cache\n\n\n";

//////////////////////////////////// static variables //////////////////////////////
static uint32_t meassureCounter = 0;

static adc_data_t adc_data_buffer[MAX_SIZE_OF_ADC_DATA_BUFFER] = {0};
static adc_data_t *adc_data_buffer_pt = adc_data_buffer;

static adc_cali_handle_t adc1_cali_handle = NULL;
static adc_oneshot_unit_handle_t adc1_handle = NULL;

// is sntp time synchronized
static bool time_synchronized = false;

// used to store asynchronous connections data
static async_resp_arg_t resp_arg[MAX_EVENT_SOCKET];

// is asynchronous connection ready
static bool async_ready = false;

//////////////////////////////////// global variables //////////////////////////////
TaskHandle_t sender_task_h = NULL;
TaskHandle_t data_receiver_task_h = NULL;
QueueHandle_t text_message_queue_h = NULL;
QueueHandle_t adc_data_queue_h = NULL;
SemaphoreHandle_t adc_data_buffer_mutex_h = NULL;

// composite channel info
node_info_t node_info = {0};

char web_buff[TEXT_QUEUE_ELEMENT_SIZE] = {0};
/////////////////////////////////// static function ////////////////////////////////
esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
        	printf("HTTP_EVENT_ERROR\n");
            break;
        case HTTP_EVENT_ON_CONNECTED:
//        	printf("HTTP_EVENT_ON_CONNECTED\n");
            break;
        case HTTP_EVENT_HEADER_SENT:
//        	printf("HTTP_EVENT_HEADER_SENT\n");
            break;
        case HTTP_EVENT_ON_HEADER:
//        	printf("HTTP_EVENT_ON_HEADER\n");
//            printf("%.*s\n", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
//        	printf("HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
//            if (!esp_http_client_is_chunked_response(evt->client)) {
//                printf("%.*s\n", evt->data_len, (char*)evt->data);
//            }
            break;
        case HTTP_EVENT_ON_FINISH:
//        	printf( "HTTP_EVENT_ON_FINISH\n");
            break;
        case HTTP_EVENT_DISCONNECTED:
//        	printf("HTTP_EVENT_DISCONNECTED\n");
            break;
        case HTTP_EVENT_REDIRECT:
        	break;
    }
    return ESP_OK;
}

static void send_data_to_all_socket(char *buff)
{
	// send data to all sockets
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (resp_arg[i].fd >= 0)
		{
			httpd_default_send(resp_arg[i].hd, resp_arg[i].fd, buff, strlen(buff), 0);
			printf("Asynchronous event sent\n");
		}
	}
}

static void channel_info_json_build(uint8_t channelNb, char *buff_out)
{
	uint8_t isActive = 0;
	uint32_t period = node_info.channel_info[channelNb].period;
	char *text_info = node_info.channel_info[channelNb].name;

	if (node_info.channel_info[channelNb].adc_read_timer_h != NULL)
	{
		isActive = xTimerIsTimerActive(node_info.channel_info[channelNb].adc_read_timer_h);
		period = pdTICKS_TO_MS(xTimerGetPeriod(node_info.channel_info[channelNb].adc_read_timer_h)) / 1000;
	}

	sprintf(buff_out, "data: {\"type\": \"channel\", \"channel\": %d, \"isActive\": %d, \"period\": %ld, \"info\": \"%s\"}\n\n",
			channelNb, isActive, period, text_info);
}

static void send_event_channel_info(uint8_t channelNb)
{
	if (async_ready)
	{
		// buffer for JSON data
		char buff_out[150];

		// build json config channel data buffer
		channel_info_json_build(channelNb, buff_out);

		// send data to all connected http client
		send_data_to_all_socket(buff_out);
	}
}

void text_json_build(char *id, char* val, char *buff_out)
{
	sprintf(buff_out, "%s=data: {\"type\": \"text\", \"id\": \"%s\", \"val\": \"%s\"}\n\n",
			JSON_DATA_SERVER_HEADER, id, val);
}

static void send_event_info(char *dataStr)
{
	// buffer for JSON data
	char buff_out[100];

	if (async_ready || strlen(node_info.server_Addr) != 0)
	{
		text_json_build("nodeInfo", dataStr, buff_out);
	}
	if (async_ready)
	{
		// send data to http client without JSON header
		send_data_to_all_socket(&buff_out[sizeof(JSON_DATA_SERVER_HEADER)]);
	}
	if (strlen(node_info.server_Addr) != 0)
	{
		// send data to data sever with JSON header
		send_data_to_data_server(buff_out);
	}
}

static void chart_name_build(adc_data_t *data, char *name)
{
	char node_name[NODE_NAME_STRING_SIZE + 1] = {0};
	char channel_name[NODE_NAME_STRING_SIZE + 1] = {0};

	if (strlen(node_info.node_name) == 0)
	{
	    esp_netif_t *netif = NULL;
	    esp_netif_ip_info_t ip;
        netif = esp_netif_next_unsafe(netif);
	    esp_netif_get_ip_info(netif, &ip);
		sprintf(node_name, IPSTR, IP2STR(&ip.ip));
	}
	else
	{
		strncpy(node_name, node_info.node_name, MIN(strlen(node_info.node_name), NODE_NAME_STRING_SIZE));
	}
	if (strlen(node_info.channel_info[data->descriptor].name) == 0)
	{
		sprintf(channel_name, "%d", data->descriptor);
	}
	else
	{
		strncpy(channel_name, node_info.channel_info[data->descriptor].name,
				MIN(strlen(node_info.channel_info[data->descriptor].name), CHANNEL_NAME_STRING_SIZE));
	}
	sprintf(name,"%s-%s", node_name, channel_name);
}

static void send_data_to_http_clients(char *buff_out)
{
	if (async_ready)
	{
		// send data to all sockets
		send_data_to_all_socket(buff_out);
	}
}

static void send_data_to_data_server(char *buff_out)
{
	if (strlen(node_info.server_Addr) != 0)
	{
		char buf_in[50] = {0};

		esp_http_client_config_t config = {
		   .url = node_info.server_Addr,
		   .event_handler = _http_event_handle,
	       .user_data = buf_in,
		};
		esp_http_client_handle_t client = esp_http_client_init(&config);
		esp_http_client_set_post_field(client, buff_out, strlen(buff_out));
		esp_err_t err = esp_http_client_perform(client);

		if (err == ESP_OK) {
			int content_length = esp_http_client_get_content_length(client);
			printf("Data sent to:%.*s, status:%d, content_len:%d\n",IP_ADDRESS_HEADER_STRING_SIZE + IP_ADDRESS_STRING_SIZE, node_info.server_Addr,
		        esp_http_client_get_status_code(client), content_length);
			if ((content_length < 0) || (content_length > sizeof(buf_in)))
				content_length = sizeof(buf_in);
			esp_http_client_read(client, buf_in, content_length);

			if (strlen(buf_in) != 0) {
				printf("Server answer: %s\n", buf_in);
			}
		}
		else
		{
			printf("http client error:%d\n", err);
		}
		esp_http_client_cleanup(client);
	}
}

static void data_json_build(adc_data_t *data, char *buff_out)
{
	char name[NODE_NAME_STRING_SIZE + CHANNEL_NAME_STRING_SIZE + 2] = {0};
	chart_name_build(data, name);
	int voltage;
	// conver raw ADC data to human readable float decimal voltage
    adc_cali_raw_to_voltage(adc1_cali_handle, data->raw_data, &voltage);

	float Vout = (float)voltage / 1000;
	// one line JSON format
	sprintf(buff_out,"%s=data: {\"type\": \"chart\", \"name\": \"%s\", \"x\": %llu, \"y\": %.3f}\n\n",
			JSON_DATA_SERVER_HEADER, name, data->time_ms, Vout);
}

static void send_all_data_from_buffer(int fd, httpd_req_t *req)
{
	// send data from buffer - adc_data_buffer[MAX_SIZE_OF_ADC_DATA_BUFFER]
	adc_data_t adc[MAX_SIZE_OF_ADC_DATA_BUFFER] = {0};

	read_adc_data_buffer(adc);

	for (uint16_t i = 0; i < MAX_SIZE_OF_ADC_DATA_BUFFER; i++)
	{
		if (adc[i].time_ms != 0)
		{
			char buff[150] = {0};
			char *buff_out = &buff[sizeof(JSON_DATA_SERVER_HEADER)];

			printf("Send data from buffer %d\n", i);
			data_json_build(&adc[i], buff);

			// send data to http server client - only data
			httpd_default_send(req->handle, fd, buff_out, strlen(buff_out), 0);
		}
	}
}

static void send_data(adc_data_t *data)
{
	char buff[150] = {0};
	char *buff_out = &buff[sizeof(JSON_DATA_SERVER_HEADER)];

	// build json data out
	data_json_build(data, buff);

	// send data to http server clients - only data
	send_data_to_http_clients(buff_out);

	// send data to web data server - data query
	send_data_to_data_server(buff);
}

static void send_configuration_data(int fd, httpd_req_t *req)
{
	char buff[150] = {0};
	char *buff_out = &buff[sizeof(JSON_DATA_SERVER_HEADER)];

	//FIXME: send empty event - temporary Microsoft edge problem fix
	send_data_to_all_socket("data: {}\n\n");

    // send front configuration data after connect
    for (uint8_t channelNb = 0; channelNb < MAX_SUPPORT_ADC_CHANNEL; channelNb++)
    {
		if (node_info.channel_info[channelNb].adc_read_timer_h != NULL)
		{
			// build json config channel data buffer
			channel_info_json_build(channelNb, buff);

			// send config data to http client
			httpd_default_send(req->handle, fd, buff, strlen(buff), 0);
		}
    }

    // send server data address node config info
    text_json_build("serverAddr", &node_info.server_Addr[IP_ADDRESS_HEADER_STRING_SIZE], buff);
    httpd_default_send(req->handle, fd, buff_out, strlen(buff_out), 0);

    // send node info data node config info
    text_json_build("nodeName", node_info.node_name, buff);
    httpd_default_send(req->handle, fd, buff_out, strlen(buff_out), 0);
}

static void close_socket(httpd_handle_t hd, int sockfd)
{
	// find socket to close it
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (sockfd == resp_arg[i].fd)
		{
			printf("Close socket fd : %d\n", sockfd);
			resp_arg[i].fd = -1;
		}
	}
	// if not active socket stop send
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (resp_arg[i].fd > 0)
		{
			return;
		}
	}
	async_ready = false;
}

static void add_socketfd_to_table(int fd, httpd_req_t *req)
{
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		if (resp_arg[i].fd < 0)
		{
			resp_arg[i].hd = req->handle;
			resp_arg[i].fd = fd;
			return;
		}
	}
}

static esp_err_t event_handler(httpd_req_t *req)
{
	int fd = httpd_req_to_sockfd(req);
    if (fd < 0) {
    	return ESP_FAIL;
    }
    add_socketfd_to_table(fd, req);
    httpd_default_send(req->handle, fd, hdr, sizeof(hdr), 0);

    // send data from adc_data_buffer[MAX_SIZE_OF_ADC_DATA_BUFFER] buffer to http client
    send_all_data_from_buffer(fd, req);

    async_ready = true;

    // send node info data to http client
    send_configuration_data(fd, req);


    return ESP_OK;
}

static void gpio_toggle(uint32_t pin)
{
	if (gpio_get_level(pin) == 1)
	{
		printf("LED on\n");
		gpio_set_level(pin, 0); // Switch pin off
	}
	else
	{
		printf("LED off\n");
		gpio_set_level(pin, 1); // Switch pin on
	}
}

static void adc_read(uint8_t adcChannel)
{
	adc_data_t adc;
	uint64_t time_ms;
	struct timeval tv;

	if (time_synchronized == false)
	{
		return;
	}

	gettimeofday(&tv, NULL);
	time_ms = tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;

	adc.time_ms = time_ms;
	adc_oneshot_read(adc1_handle, adcChannel, &adc.raw_data);

	// additional information about data channel
	adc.descriptor = adcChannel;

    if( xQueueSend( adc_data_queue_h, ( void * ) &adc, ( TickType_t ) 0 ) == pdPASS )
    {
    	time_t raw_time;
    	char strftime_buf[10];
    	struct tm timeinfo;

    	time(&raw_time);
    	localtime_r(&raw_time, &timeinfo);
    	sprintf(strftime_buf, "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

    	// For boards with eFuse ADC calibration bits, esp_adc_cal_raw_to_voltage()
    	// can be used to get the calibrated conversion results.
    	// voltage 	Vout = read_raw
    	// max 		1.1V = 4095 (12 bits)
    	// The input voltage in above example is from 0 to 1.1V (0 dB attenuation)
    	// float Vout = (read_raw * 1.1) / (0xFFF - 1);
    	int voltage;
    	adc_cali_raw_to_voltage(adc1_cali_handle, adc.raw_data, &voltage);
    	float Vout = (float)adc.raw_data / 1000;

    	printf("%s:ADC1(%d):%.2f[V]\n", strftime_buf, adcChannel, Vout );

    	if (data_receiver_task_h)
    	{
			xTaskNotify( data_receiver_task_h,
						ADC_DATA_EVENT,
						eSetBits);
    	}
    }
}

static bool adc_calibration_init(adc_unit_t unit, adc_channel_t channel, adc_atten_t atten, adc_cali_handle_t *out_handle)
{
    esp_err_t ret = ESP_FAIL;
    bool calibrated = false;
    adc_cali_handle_t handle = NULL;

#if ADC_CALI_SCHEME_CURVE_FITTING_SUPPORTED
    if (!calibrated) {
    	printf("calibration scheme version is %s", "Curve Fitting\n");
        adc_cali_curve_fitting_config_t cali_config = {
            .unit_id = unit,
            .chan = channel,
            .atten = atten,
            .bitwidth = ADC_BITWIDTH_DEFAULT,
        };
        ret = adc_cali_create_scheme_curve_fitting(&cali_config, &handle);
        if (ret == ESP_OK) {
            calibrated = true;
        }
    }
#endif

#if ADC_CALI_SCHEME_LINE_FITTING_SUPPORTED
    if (!calibrated) {
        printf("calibration scheme version is %s", "Line Fitting\n");
        adc_cali_line_fitting_config_t cali_config = {
            .unit_id = unit,
            .atten = atten,
            .bitwidth = ADC_BITWIDTH_DEFAULT,
        };
        ret = adc_cali_create_scheme_line_fitting(&cali_config, &handle);
        if (ret == ESP_OK) {
            calibrated = true;
        }
    }
#endif

    *out_handle = handle;
    if (ret == ESP_OK) {
        printf("Calibration Success\n");
    } else if (ret == ESP_ERR_NOT_SUPPORTED || !calibrated) {
    	printf("eFuse not burnt, skip software calibration\n");
    } else {
    	printf("Invalid arg or no memory\n");
    }

    return calibrated;
}

static void pxADC_read_data_timer_cb( TimerHandle_t xTimer )
{
	uint32_t channel = ( uint32_t ) pvTimerGetTimerID( xTimer );
	adc_read(channel);
}

static void apply_configuration(void)
{
	for (uint32_t channelNb = 0; channelNb < MAX_SUPPORT_ADC_CHANNEL; channelNb++)  // conversion success
	{
		// channel was used
		if (node_info.channel_info[channelNb].adc_read_timer_h != NULL)
		{
//			adc1_config_channel_atten(channelNb, ADC_ATTEN_DB_11);    <- depreciated

			// create timer used to sampling data
			node_info.channel_info[channelNb].adc_read_timer_h = xTimerCreate("ADCReadTimer",
																	pdMS_TO_TICKS( node_info.channel_info[channelNb].period * 1000 ),
																	pdTRUE,
																	(void *)channelNb,
																	pxADC_read_data_timer_cb);

			assert(node_info.channel_info[channelNb].adc_read_timer_h != NULL);

		    //-------------ADC1 Config---------------//
		    adc_oneshot_chan_cfg_t config = {
		        .bitwidth = ADC_BITWIDTH_DEFAULT,
		        .atten = ADC_ATTEN,
		    };
		    adc_oneshot_config_channel(adc1_handle, channelNb, &config);

		    //-------------ADC1 Calibration Init---------------//
		    adc_calibration_init(ADC_UNIT_1, channelNb, ADC_ATTEN, &adc1_cali_handle);

			// start timer
			xTimerStart(node_info.channel_info[channelNb].adc_read_timer_h, 10);
		}
	}
}

static void IRAM_ATTR isr_handler(void *arg)
{
	char buff[TEXT_QUEUE_ELEMENT_SIZE];
    sprintf(buff, "Node:%.*s:GPIO ISR info pin(%d) = %d", NODE_NAME_STRING_SIZE, node_info.node_name, BUTTON_PIN, gpio_get_level(BUTTON_PIN));

    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if( data_receiver_task_h &&
    	xQueueSendFromISR(text_message_queue_h, buff, &xHigherPriorityTaskWoken) == pdPASS )
    {
    	xTaskNotifyFromISR( data_receiver_task_h,
    						TEXT_MESSAGE_EVENT,
							eSetBits,
							&xHigherPriorityTaskWoken );

    	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }
}

static void add_adc_data_to_buffer(adc_data_t *data)
{
	/* See if we can obtain the semaphore.  If the semaphore is not
	available wait 10 ticks to see if it becomes free. */
	if( xSemaphoreTake( adc_data_buffer_mutex_h, ( TickType_t ) 10 ) == pdTRUE )
	{
		// add data field
		adc_data_buffer_pt->raw_data = data->raw_data;
		// add time filed
		adc_data_buffer_pt->time_ms = data->time_ms;
		// add descriptor field
		adc_data_buffer_pt->descriptor = data->descriptor;

		if (++adc_data_buffer_pt >= adc_data_buffer + MAX_SIZE_OF_ADC_DATA_BUFFER)
		{
			adc_data_buffer_pt = adc_data_buffer;
		}
		xSemaphoreGive( adc_data_buffer_mutex_h);
	}
}

static void read_adc_data_buffer(adc_data_t *buffer)
{
	if (buffer != NULL)
	{
		/* See if we can obtain the semaphore.  If the semaphore is not
		available wait 10 ticks to see if it becomes free. */
		if( xSemaphoreTake( adc_data_buffer_mutex_h, ( TickType_t ) 10 ) == pdTRUE )
		{
			uint32_t pos_to_end = adc_data_buffer + MAX_SIZE_OF_ADC_DATA_BUFFER - adc_data_buffer_pt;
			uint32_t pos_from_begin = adc_data_buffer_pt - adc_data_buffer;


			if (pos_to_end > 0)
			{
				memcpy(	buffer,
						adc_data_buffer_pt,
						pos_to_end * sizeof(adc_data_buffer[0]));
			}

			if (pos_from_begin > 0)
			{
				memcpy(	buffer + pos_to_end,
						adc_data_buffer,
						pos_from_begin * sizeof(adc_data_buffer[0]));
			}

			xSemaphoreGive( adc_data_buffer_mutex_h);
		}
	}
}

static void sender_task( void * pvParameters )
{
	for( ;; )
	{
		char buff[TEXT_QUEUE_ELEMENT_SIZE];
		sprintf(buff, "Node:%.*s info nb:%ld -> Pin(%d) = %d", NODE_NAME_STRING_SIZE, node_info.node_name,
						meassureCounter++, BUTTON_PIN, gpio_get_level(BUTTON_PIN));

		if(data_receiver_task_h &&
			xQueueSend( text_message_queue_h, ( void * ) buff, ( TickType_t ) portMAX_DELAY ) == pdPASS )
		{
//			printf("Sender added %d elements to queue, message:%s\n", uxQueueMessagesWaiting(text_message_queue_h), buff );
		   	xTaskNotify( data_receiver_task_h,
		    			 TEXT_MESSAGE_EVENT,
						 eSetBits);
		}
		vTaskDelay(pdMS_TO_TICKS( 10000 ));
	}
}

static void data_receiver_task( void * pvParameters )
{
	const TickType_t xMaxBlockTime = pdMS_TO_TICKS( 5 );
	BaseType_t xResult;
	uint32_t ulNotifiedValue = 0;

	for( ;; )
	{
		/* Wait to be notified of an interrupt. */
		xResult = xTaskNotifyWait( pdFALSE,    			/* Don't clear bits on entry. */
		                           ULONG_MAX,       	/* Clear all bits on exit. */
		                           &ulNotifiedValue, 	/* Stores the notified value. */
								   portMAX_DELAY );
		if (xResult == pdPASS)
		{
			/* A notification was received.  See which bits were set. */
			if( ( ulNotifiedValue & TEXT_MESSAGE_EVENT ) != 0 )
			{
				while(uxQueueMessagesWaiting(text_message_queue_h))
				{
					if( xQueueReceive( text_message_queue_h, web_buff, ( TickType_t ) xMaxBlockTime ) == pdFAIL )
					{
						printf("Text message queue receive fail\n");
						xQueueReset(text_message_queue_h);
					}
					else
					{
						// send info event to http client and web data server in JSON format
						send_event_info(web_buff);
					}
				}
			}
			/* A notification was received.  See which bits were set. */
			if( ( ulNotifiedValue & ADC_DATA_EVENT ) != 0 )
			{
				while(uxQueueMessagesWaiting(adc_data_queue_h))
				{
					adc_data_t adc_data;
					if( xQueueReceive( adc_data_queue_h, &adc_data, ( TickType_t ) xMaxBlockTime ) == pdFAIL )
					{
						printf("ADC data queue receive fail\n");
						xQueueReset(adc_data_queue_h);
					}
					else
					{
					    // add date to buffer
						add_adc_data_to_buffer(&adc_data);
					   	// send last read ADC data to http client and web data server in JSON format
					    send_data(&adc_data);
					}
				}
			}
		}
	}
 }

static void on_got_ip(void *arg, esp_event_base_t event_base,
                      int32_t event_id, void *event_data)
{
	if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
	{
		ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
		printf("IP address:%d:%d:%d:%d\n",IP2STR(&event->ip_info.ip));
	}
}

static void led_set_state(uint8_t led_pin, char *param)
{
	printf("Found URL query parameter => %s\n", param);
	if (strcmp(param, "toggle") == 0) {
		gpio_toggle(led_pin);
	}
	else if (strcmp(param, "reset") == 0) {
		gpio_set_level(led_pin, 0);
	} // Switch pin off
	else if (strcmp(param, "set") == 0) {
		gpio_set_level(led_pin, 1);
	} // Switch pin on}
}

static esp_err_t cmd_get_handler(httpd_req_t *req)
{
    char*  buf;
    size_t buf_len;

    printf("cmd query:%s\n", req->uri);

	buf_len = httpd_req_get_url_query_len(req) + 1;
	if (buf_len > 1)
	{
		buf = malloc(buf_len);
		if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK)
		{
			printf("Found URL query => %s\n", buf);
			char param[32];
			/* Get value of expected key from query string */
			if (httpd_query_key_value(buf, "led1", param, sizeof(param)) == ESP_OK)
			{
				led_set_state(LED1_GPIO, param);
			}
			if (httpd_query_key_value(buf, "led2", param, sizeof(param)) == ESP_OK)
			{
				led_set_state(LED2_GPIO, param);
			}
			if (httpd_query_key_value(buf, "channel", param, sizeof(param)) == ESP_OK)
			{
				char * pt = param;
				uint32_t channelNb = strtol(param, &pt, 10);
				if ((pt != param) && (channelNb < MAX_SUPPORT_ADC_CHANNEL))  // conversion success
				{
					if (httpd_query_key_value(buf, "info", param, sizeof(param)) == ESP_OK)
					{
						// safe copy to chaneel info buffer
						memset(node_info.channel_info[channelNb].name, 0 , CHANNEL_NAME_STRING_SIZE);
						strncpy(node_info.channel_info[channelNb].name, param, CHANNEL_NAME_STRING_SIZE - 1);

						printf("Add channel %ld - %s\n", channelNb, node_info.channel_info[channelNb].name);
					    //-------------ADC1 Config---------------//
					    adc_oneshot_chan_cfg_t config = {
					        .bitwidth = ADC_BITWIDTH_DEFAULT,
					        .atten = ADC_ATTEN,
					    };
					    adc_oneshot_config_channel(adc1_handle, channelNb, &config);

					    //-------------ADC1 Calibration Init---------------//
					    adc_calibration_init(ADC_UNIT_1, channelNb, ADC_ATTEN, &adc1_cali_handle);

					}
					// send data to http client front reconfiguration
					send_event_channel_info(channelNb);
				}
			}
			if (httpd_query_key_value(buf, "start", param, sizeof(param)) == ESP_OK)
			{
				char * pt = NULL;
				uint32_t channelNb =  strtol(param, &pt, 10);
				if ((pt > param) && (channelNb < MAX_SUPPORT_ADC_CHANNEL))  // conversion success
				{
					uint32_t time = 0;
					if (httpd_query_key_value(buf, "time", param, sizeof(param)) == ESP_OK)
					{
						if (pt > param)
						{
							time =  strtol(param, &pt, 10);

							// Create timer and set period
							node_info.channel_info[channelNb].period = time;

							if (node_info.channel_info[channelNb].adc_read_timer_h == NULL)
							{
//								adc1_config_channel_atten(channelNb, ADC_ATTEN_DB_11);  <-depreciated

								node_info.channel_info[channelNb].adc_read_timer_h = xTimerCreate("ADCReadTimer",
																						pdMS_TO_TICKS( time * 1000 ),
																						pdTRUE,
																						(void *)channelNb,
																						pxADC_read_data_timer_cb);
								assert(node_info.channel_info[channelNb].adc_read_timer_h != NULL);
							}
							else
							{
								// change timer period
								xTimerChangePeriod(node_info.channel_info[channelNb].adc_read_timer_h, pdMS_TO_TICKS( time * 1000 ), 10);
							}
							xTimerStart(node_info.channel_info[channelNb].adc_read_timer_h, 10);
						}
					}
				}
			}
			if (httpd_query_key_value(buf, "stop", param, sizeof(param)) == ESP_OK)
			{
				char * pt = param;
				uint32_t channelNb =  strtol(param, &pt, 10);
				if ((pt > param) && (channelNb < MAX_SUPPORT_ADC_CHANNEL))  // conversion success
				{
					// Stop and destroy timer
					if (node_info.channel_info[channelNb].adc_read_timer_h != NULL)
					{
						xTimerStop(node_info.channel_info[channelNb].adc_read_timer_h, 10);
						xTimerDelete(node_info.channel_info[channelNb].adc_read_timer_h, 10);
					}
					node_info.channel_info[channelNb].adc_read_timer_h = NULL;
				}
			}
			if (httpd_query_key_value(buf, "serverAddr", param, sizeof(param)) == ESP_OK)
			{
				// server address for data send
				memset(node_info.server_Addr, 0 , IP_ADDRESS_HEADER_STRING_SIZE + IP_ADDRESS_STRING_SIZE);
				strncpy(node_info.server_Addr, "http://", IP_ADDRESS_HEADER_STRING_SIZE + 1);
				strncpy(node_info.server_Addr + IP_ADDRESS_HEADER_STRING_SIZE, param, MIN(strlen(param), IP_ADDRESS_STRING_SIZE));

				printf("New data server address: %.*s\n", IP_ADDRESS_HEADER_STRING_SIZE + IP_ADDRESS_STRING_SIZE, node_info.server_Addr);
			}
			if (httpd_query_key_value(buf, "nodeName", param, sizeof(param)) == ESP_OK)
			{
				// node name
				memset(node_info.node_name, 0 , NODE_NAME_STRING_SIZE);
				strncpy(node_info.node_name, param, MIN(strlen(param), NODE_NAME_STRING_SIZE));
				set_mdns();

				printf("New node name: %.*s\n", NODE_NAME_STRING_SIZE, node_info.server_Addr);
			}
			if (httpd_query_key_value(buf, "save", param, sizeof(param)) == ESP_OK)
			{
				// save configuration
				save_configuration(&node_info, sizeof(node_info));
			}
		}
	}
	httpd_resp_sendstr(req, "OK");
    return ESP_OK;
}

static void wifi_start(void)
{
	static struct file_server_data *server_data = NULL;

	// reset all slot - connection socket table
	for (uint8_t i = 0; i < MAX_EVENT_SOCKET; i++)
	{
		resp_arg[i].fd = -1;
	}
	printf("WiFi Initialization ...\n");

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                         IP_EVENT_STA_GOT_IP,
														 &on_got_ip,
                                                         NULL,
                                                         &instance_got_ip));

	wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_EXAMPLE_WIFI_SSID,
            .password = CONFIG_EXAMPLE_WIFI_PASSWORD,
            .scan_method = WIFI_ALL_CHANNEL_SCAN,
			.threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    printf("Connecting to %s...\n", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    esp_wifi_connect();

    // http server
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    //should be increased due to "A stack overflow in task httpd has been detected"
    config.stack_size = 10240;
    config.lru_purge_enable = true;
    /* Use the URI wildcard matching function in order to
	 * allow the same handler to respond to multiple different
	 * target URIs which match the wildcard scheme */
	config.uri_match_fn = httpd_uri_match_wildcard;
	config.close_fn = close_socket;

    server_data = calloc(1, sizeof(struct file_server_data));
    if (server_data == NULL)
    {
    	return;
    }
    strlcpy(server_data->base_path, "/spiffs", sizeof(server_data->base_path));

    // Start the httpd server
    printf("Starting server on port: '%d'\n", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
//      Set URI handlers
    	/* URI handler for getting uploaded files */
    	httpd_uri_t file_download = {
    		.uri       = "/*",  // Match all URIs of type /path/to/file
    		.method    = HTTP_GET,
    		.handler   = download_get_handler,
    		.user_ctx  = server_data
    	};

    	/* URI handler for uploading files to server */
    	httpd_uri_t file_upload = {
    		.uri       = "/upload/*",   // Match all URIs of type /upload/path/to/file
    		.method    = HTTP_POST,
    		.handler   = upload_post_handler,
    		.user_ctx  = server_data
    	};

    	/* URI handler for deleting files from server */
    	httpd_uri_t file_delete = {
    		.uri       = "/delete/*",   // Match all URIs of type /delete/path/to/file
    		.method    = HTTP_POST,
    		.handler   = delete_post_handler,
    		.user_ctx  = server_data
    	};
    	/* Set device configuration */
    	static const httpd_uri_t cmd_req = {
    	    .uri       = "/cmd*",
    	    .method    = HTTP_GET,
    	    .handler   = cmd_get_handler,
    	    .user_ctx  = NULL
    	};
    	/* Asynchronous connection request */
    	static const httpd_uri_t event_req = {
    		.uri       = "/event*",
			.method    = HTTP_GET,
			.handler   = event_handler,
			.user_ctx  = NULL
    	};

        printf("Registering URI handlers\n");
//		command control request
        httpd_register_uri_handler(server, &cmd_req);
//      asynchronous connection request
        httpd_register_uri_handler(server, &event_req);
//      file system support
        httpd_register_uri_handler(server, &file_download);
        httpd_register_uri_handler(server, &file_upload);
        httpd_register_uri_handler(server, &file_delete);
    }
}

static void time_synchronized_cb(struct timeval *tv)
{
	printf("Time synchronized\n");
	//TODO: check problem send info
	//send_event_info( 10, "SNTP time synchronized");
	time_synchronized = true;
}

static void set_mdns(void)
{
	if (strlen(node_info.node_name) > 0)
	{
		//set hostname
		mdns_hostname_set(node_info.node_name);
		//set default instance
		mdns_instance_name_set(node_info.node_name);
	}
	else
	{
		//set hostname
		mdns_hostname_set("ktp");
		//set default instance
		mdns_instance_name_set("ktp");
	}
}

///////////////////////////////////////// main function ///////////////////////////////////

void app_main(void)
{
	printf("ESP32 started ...\n");
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // file system init
    ESP_ERROR_CHECK(init_spiffs());

    printf("GPIO Initialization ...\n");
    /* Configure the IOMUX register for pad LEDs GPIO */
    gpio_reset_pin(LED1_GPIO);
    gpio_reset_pin(LED2_GPIO);

    /* Set the GPIO as a push/pull output */
    gpio_set_direction(LED1_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_direction(LED2_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_direction(BUTTON_PIN, GPIO_MODE_INPUT);

    // gpio interrupt
    gpio_set_intr_type(BUTTON_PIN, GPIO_INTR_ANYEDGE);
	gpio_install_isr_service(0);
    gpio_isr_handler_add(BUTTON_PIN, isr_handler, (void *)BUTTON_PIN);

    printf("ADC Initialization ...\n");
    //	adc init
    //-------------ADC1 Init---------------//
    adc_oneshot_unit_init_cfg_t init_config1 = {
        .unit_id = ADC_UNIT_1,
    };
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config1, &adc1_handle));

	// @note  ESP32 only supports output of ADC2's internal reference voltage.
	// - ESP_ERR_INVALID_ARG: Unsupported GPIO
    // enable routing ADC reference voltage to external GPIO
	// Routing ADC reference voltage to GPIO, so it can be manually measured (for Default Vref)

    // Deprecated //
//    esp_err_t status = adc_vref_to_gpio(ADC_UNIT_1, GPIO_NUM_25);
//    if (status == ESP_OK) {
//        printf("V_ref routed to GPIO\n");
//    } else  if (status == ESP_ERR_INVALID_ARG){
//        printf("Unsupported GPIO for ADC%d\n", ADC_UNIT_1);
//    }


    // Queue create
    /* Create a queue capable of containing 10 unsigned long values. */
    text_message_queue_h = xQueueCreate( TEXT_QUEUE_SIZE, TEXT_QUEUE_ELEMENT_SIZE);
    assert(text_message_queue_h != NULL);

    /* Create a queue capable of containing 10 unsigned long values. */
	adc_data_queue_h = xQueueCreate( ADC_DATA_QUEUE_SIZE, ADC_DATA_QUEUE_ELEMENT_SIZE);
	assert(adc_data_queue_h != NULL);

    /* Attempt to create a semaphore. */
    adc_data_buffer_mutex_h = xSemaphoreCreateMutex();
    assert(adc_data_buffer_mutex_h != NULL);

	// Create dat receiver task
    xTaskCreate( data_receiver_task, "DataReceiverTask", 3000, NULL, tskIDLE_PRIORITY, &data_receiver_task_h );
    assert(data_receiver_task_h != NULL);

    // Create text message sender task
	xTaskCreate( sender_task, "SenderTask", 2000, NULL, tskIDLE_PRIORITY + 1, &sender_task_h );
	assert(sender_task_h != NULL);

    // configuration read
    printf("Read configuration ...\n");
    read_configuration(&node_info, sizeof(node_info));
    apply_configuration();

    // start wifi with simple http server implementation
	wifi_start();

    printf("SNTP initialization ...\n");
    // synchronization via SNTP
    setenv("TZ", "UTC-1", 1);
    tzset();
    sntp_set_time_sync_notification_cb(time_synchronized_cb);
    esp_sntp_setoperatingmode(SNTP_OPMODE_POLL);
    esp_sntp_setservername(0, "ntp.nask.pl");
    esp_sntp_init();

    printf("Mdns Initialization ...\n");
	//initialize mDNS service
	esp_err_t err = mdns_init();
	if (err)
	{
		printf("MDNS Init failed: 0x%X\n", err);
	}
	else
	{
		set_mdns();
	}
}

void web_data_formater(char *buff)
{
	uint16_t i_out = 0;

	adc_data_t adc[MAX_SIZE_OF_ADC_DATA_BUFFER] = {0};

	read_adc_data_buffer(adc);

	for (uint16_t i = 0; i < MAX_SIZE_OF_ADC_DATA_BUFFER; i++)
	{
		if (adc[i].time_ms != 0)
		{
//			char str_time_buf[10];
//			struct tm timeinfo;

//			localtime_r(&adc[i].time, &timeinfo);
//			strftime(str_time_buf, sizeof(strftime_buf), "%c", &timeinfo);
//		   	sprintf(str_time_buf, "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
//			sprintf(&buff[i*47], "<p>%s:ADC1(4):%.2f[V]</p>", str_time_buf, Vout );
			int voltage;
			adc_cali_raw_to_voltage(adc1_cali_handle, adc[i].raw_data, &voltage);
			float Vout = voltage / 1000;
			sprintf(&buff[i_out*28], "[%20lld,%.2f],", adc[i].time_ms, Vout );
			i_out++;
		}
	}
}

